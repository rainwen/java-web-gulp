<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    request.setAttribute("root",path);
%>
<html>
<head>
    <title>order index</title>
    <link href="${root}/resources/css/global.css" rel="stylesheet" type="text/css" />
    <link href="${root}/resources/css/order.css" rel="stylesheet" type="text/css" />
    <%@ include file="/commons/script.jsp" %>
    <script src="${root}/resources/js/commons/plus/date/WdatePicker.js" type="text/javascript"></script>
    <script src="${root}/resources/js/order/order_index.js" type="text/javascript"></script>
</head>
<body>
order index
</body>
</html>
