<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    request.setAttribute("root",path);
%>
<html>
<head>
    <title>account index</title>
    <link href="${root}/resources/css/global.css" rel="stylesheet" type="text/css" />
    <link href="${root}/resources/css/account.css" rel="stylesheet" type="text/css" />
    <%@include file="/commons/script.jsp"%>
    <script src="${root}/resources/js/commons/plus/date/WdatePicker.js" type="text/javascript"></script>
    <script src="${root}/resources/js/account/account_index.js" type="text/javascript"></script>
</head>
<body>
account index
</body>
</html>
